package com.example.phonebook.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "phone_types")
public class PhoneTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false, unique = true)
    private int id;

    @Column(name = "name")
    private String name;

    @OneToMany(targetEntity = PhoneNumberEntity.class, mappedBy = "type")
    private Set<PhoneNumberEntity> numbers;

    public PhoneTypeEntity() {
        this.numbers = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<PhoneNumberEntity> getNumbers() {
        return numbers;
    }

    public void setNumbers(Set<PhoneNumberEntity> numbers) {
        this.numbers = numbers;
    }
}

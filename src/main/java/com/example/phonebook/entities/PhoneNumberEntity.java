package com.example.phonebook.entities;

import javax.persistence.*;

@Entity
@Table(name = "phone_numbers")
public class PhoneNumberEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "number")
    private String number;

    @ManyToOne(targetEntity = ContactEntity.class)
    @JoinColumn(name = "person_id")
    private ContactEntity owner;

    @ManyToOne(targetEntity = PhoneTypeEntity.class)
    @JoinColumn(name = "type_id")
    private PhoneTypeEntity type;

    public PhoneNumberEntity() {
    }

    public PhoneNumberEntity(String number, ContactEntity owner, PhoneTypeEntity type) {
        this.number = number;
        this.owner = owner;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ContactEntity getOwner() {
        return owner;
    }

    public void setOwner(ContactEntity owner) {
        this.owner = owner;
    }

    public PhoneTypeEntity getType() {
        return type;
    }

    public void setType(PhoneTypeEntity type) {
        this.type = type;
    }
}

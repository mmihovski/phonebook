package com.example.phonebook.services;

import com.example.phonebook.configurations.StorageProperties;
import com.example.phonebook.exceptions.StorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
public class StorageService {

    private final Path rootLocation;

    @Autowired
    public StorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @PostConstruct
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage location");
        }
    }

    public void storeFile(MultipartFile file, String fileName) {
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, this.rootLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Failed to store file: " + fileName);
        }
    }

    public String loadPath(String filename) {

        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return file.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return "FILE_NOT_FOUND";
    }

    public void deleteFile(String fileName) {
        Path file = rootLocation.resolve(fileName);
        try {
            FileSystemUtils.deleteRecursively(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.example.phonebook.services;

import com.example.phonebook.entities.RoleEntity;
import com.example.phonebook.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    public void seedRolesInDb(){
        if(roleRepository.count() == 0){
            RoleEntity roleEntity = new RoleEntity();
            roleEntity.setRole("USER");
            roleRepository.save(roleEntity);
        }
    }
}

package com.example.phonebook.services;

import com.example.phonebook.bindingModels.ContactBindingModel;
import com.example.phonebook.bindingModels.PhoneNumberBindingModel;
import com.example.phonebook.entities.ContactEntity;
import com.example.phonebook.entities.PhoneNumberEntity;
import com.example.phonebook.entities.PhoneTypeEntity;
import com.example.phonebook.entities.UserEntity;
import com.example.phonebook.exceptions.ItemNotFoundException;
import com.example.phonebook.repositories.ContactRepository;
import com.example.phonebook.repositories.PhoneNumberRepository;
import com.example.phonebook.repositories.PhoneTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ContactsService {

    private final PhoneTypeRepository phoneTypeRepository;
    private final ContactRepository contactRepository;
    private final PhoneNumberRepository phoneNumberRepository;
    private UserService userService;
    private StorageService storageService;

    @Autowired
    public ContactsService(PhoneTypeRepository phoneTypeRepository, ContactRepository contactRepository, PhoneNumberRepository phoneNumberRepository) {
        this.phoneTypeRepository = phoneTypeRepository;
        this.contactRepository = contactRepository;
        this.phoneNumberRepository = phoneNumberRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setStorageService(StorageService storageService) {
        this.storageService = storageService;
    }

    @Transactional
    public void createContact(ContactBindingModel contactBindingModel) {

        ContactEntity contact = new ContactEntity();

        convertContactBindingModelToContactEntity(contactBindingModel, contact);

        contactRepository.save(contact);

        for (PhoneNumberEntity number : contact.getNumbers()) {
            phoneNumberRepository.save(number);
        }
    }

    @Transactional
    public void updateContact(int id, ContactBindingModel contactBindingModel) {

        ContactEntity contact = findById(id);
        deleteAllNumbers(id);
        deletePictureByContactId(id);

        convertContactBindingModelToContactEntity(contactBindingModel, contact);

        for (PhoneNumberEntity number : contact.getNumbers()) {
            phoneNumberRepository.saveAndFlush(number);
        }
        contactRepository.save(contact);

    }

    @Transactional
    public void deleteContact(int id) {
        deletePictureByContactId(id);
        contactRepository.deleteById(id);
    }

    @Transactional
    public void deleteAllNumbers(int id) {
        //contactRepository.findById(id).ifPresent(phoneNumberRepository::deleteAllByOwner);

        ContactEntity contactEntity = findById(id);
        phoneNumberRepository.deleteAllByOwner(contactEntity);
    }


    public ContactBindingModel getContactById(int id) {
        return convertContactEntityToContactBM(findById(id));
    }

    public List<ContactBindingModel> getAllContacts(Principal principal) {
        return convertContactsList(contactRepository.findAllByOwnerUsername(principal.getName()));
    }

    public List<ContactBindingModel> getAllContactsSortByFirstName(Principal principal) {
        return convertContactsList(contactRepository.findAllByOwnerUsernameOrderByFirstName(principal.getName()));
    }

    public List<ContactBindingModel> getAllContactsSortByLastName(Principal principal) {
        return convertContactsList(contactRepository.findAllByOwnerUsernameOrderByLastName(principal.getName()));
    }

    public List<ContactBindingModel> searchContact(String name) {
        //return convertContactsList(contactRepository.findByFirstName(name));
        return convertContactsList(contactRepository.findAllByName(name));
    }

    public List<String> findAllPhoneTypes() {
        List<PhoneTypeEntity> phoneTypes = phoneTypeRepository.findAll();
        List<String> types = new ArrayList<>();

        for (PhoneTypeEntity phoneType : phoneTypes) {
            types.add(phoneType.getName());
        }

        return types;
    }

    private ContactEntity findById(int id) {
        ContactEntity contact = contactRepository.findById(id).orElse(null);

        if (contact == null) {
            throw new ItemNotFoundException("Contact Not Found");
        }

        return contact;
    }

    private void deletePictureByContactId(int id ){
        ContactBindingModel contact = getContactById(id);
        if(contact.getPictureName() != null) {
            storageService.deleteFile(contact.getPictureName());
        }
    }

    private List<ContactBindingModel> convertContactsList(List<ContactEntity> contactsList) {
        List<ContactBindingModel> contactBindingModelList = new ArrayList<>();

        for (ContactEntity contact : contactsList) {
            contactBindingModelList.add(convertContactEntityToContactBM(contact));
        }

        return contactBindingModelList;
    }

    private ContactBindingModel convertContactEntityToContactBM(ContactEntity contact) {

        ContactBindingModel contactBindingModel = new ContactBindingModel();

        contactBindingModel.setId(contact.getId());
        contactBindingModel.setFirstName(contact.getFirstName());
        contactBindingModel.setLastName(contact.getLastName());
        contactBindingModel.setAddress(contact.getAddress());
        contactBindingModel.setEmail(contact.getEmail());
        contactBindingModel.setNotes(contact.getNotes());
        contactBindingModel.setWebsite(contact.getWebsite());
        contactBindingModel.setPictureName(contact.getPictureName());

        if (contact.getBirthday() != null) {
            contactBindingModel.setBirthday(String.valueOf(contact.getBirthday()));
        } else {
            contactBindingModel.setBirthday("");
        }

        List<PhoneNumberBindingModel> numbers = new ArrayList<>();

        for (PhoneNumberEntity number : contact.getNumbers()) {
            PhoneNumberBindingModel phoneNumberBindingModel = new PhoneNumberBindingModel();
            phoneNumberBindingModel.setNumber(number.getNumber());

            if (number.getType() == null) {
                phoneNumberBindingModel.setType("");
            } else {
                phoneNumberBindingModel.setType(number.getType().getName());
            }

            numbers.add(phoneNumberBindingModel);
        }

        contactBindingModel.setNumbers(numbers);

        return contactBindingModel;
    }

    private void convertContactBindingModelToContactEntity(ContactBindingModel contactBindingModel, ContactEntity contact) {

        UserEntity user = userService.findByUsername(contactBindingModel.getUsername());

        Set<PhoneNumberEntity> numbers = contact.getNumbers();
        numbers.clear();

        for (PhoneNumberBindingModel phoneNumberBindingModel : contactBindingModel.getNumbers()) {
            if (!phoneNumberBindingModel.getNumber().equals("")) {
                PhoneTypeEntity type = phoneTypeRepository.findByName(phoneNumberBindingModel.getType());
                numbers.add(new PhoneNumberEntity(phoneNumberBindingModel.getNumber(), contact, type));
            }
        }

        if(!contactBindingModel.getPicture().isEmpty()) {
            String fileName = contactBindingModel.getUsername() + contactBindingModel.getFirstName() + contactBindingModel.getLastName() + ".jpeg";
            storageService.storeFile(contactBindingModel.getPicture(), fileName);
            contact.setPictureName(fileName);
        }

        if (!contactBindingModel.getBirthday().isEmpty()) {
            contact.setBirthday(LocalDate.parse(contactBindingModel.getBirthday()));
        } else {
            contact.setBirthday(null);
        }

        contact.setOwner(user);
        contact.setFirstName(contactBindingModel.getFirstName());
        contact.setLastName(contactBindingModel.getLastName());
        contact.setEmail(contactBindingModel.getEmail());
        contact.setWebsite(contactBindingModel.getWebsite());
        contact.setAddress(contactBindingModel.getAddress());
        contact.setNotes(contactBindingModel.getNotes());
        contact.setNumbers(numbers);
    }
}

package com.example.phonebook.services;

import com.example.phonebook.bindingModels.UserBindingModel;
import com.example.phonebook.entities.PasswordToken;
import com.example.phonebook.entities.RoleEntity;
import com.example.phonebook.entities.UserEntity;
import com.example.phonebook.exceptions.ItemNotFoundException;
import com.example.phonebook.repositories.PasswordTokenRepository;
import com.example.phonebook.repositories.RoleRepository;
import com.example.phonebook.repositories.UserRepository;
import com.example.phonebook.utils.EmailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordTokenRepository passwordTokenRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final EmailSender emailSender;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordTokenRepository passwordResetTokenRepository, BCryptPasswordEncoder bCryptPasswordEncoder, EmailSender emailSender) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordTokenRepository = passwordResetTokenRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.emailSender = emailSender;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = findByUsername(username);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (RoleEntity role : user.getAuthorities()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getRole()));
        }

        return new User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }

    public void registerUser(UserBindingModel userBindingModel) {
        saveUser(userBindingModel, new UserEntity());
    }

    public void changePassword(UserBindingModel userBindingModel) {
        saveUser(userBindingModel, findByUsername(userBindingModel.getUsername()));
    }

    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    public UserBindingModel getUserByUsername(String username) {
        return convertUserEntityToUserBM(findByUsername(username));
    }

    public UserBindingModel getUserByToken(String token) {
        PasswordToken passwordResetToken = passwordTokenRepository.findByConfirmationToken(token);
        return convertUserEntityToUserBM(findByUsername(passwordResetToken.getUser().getUsername()));
    }

    public UserEntity findByUsername(String username) {

        UserEntity user = userRepository.findByUsername(username);

        if (user == null) {
            throw new ItemNotFoundException("User" + username + "not found!");
        }

        return user;
    }

    @Transactional
    public void createTokenAndSendEmail(String email) {

        PasswordToken passwordResetToken = new PasswordToken(findByEmail(email));
        passwordTokenRepository.save(passwordResetToken);

        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setTo(email);
        mailMessage.setSubject("Complete Password Reset!");
        mailMessage.setFrom("test-email@test.com");
        mailMessage.setText("To complete the password reset process, please follow the link:\n"
                + "http://localhost:8080/reset-password?token=" + passwordResetToken.getConfirmationToken() +
                "\n WARNING! This link is available only once and it is valid for 20 minutes!");

        emailSender.sendEmail(mailMessage);
    }

    @Transactional
    public boolean isTokenValid(String token) {
        PasswordToken passwordResetToken = passwordTokenRepository.findByConfirmationToken(token);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        if (passwordResetToken != null && calendar.getTime().before(addMinutesToJavaUtilDate(passwordResetToken.getCreatedDate(), 2))) {
            return true;
        } else {
            deleteToken(token);
            return false;
        }
    }

    @Transactional
    public void deleteToken(String token) {
        passwordTokenRepository.deleteByConfirmationToken(token);
    }

    @Transactional
    public void deleteUser(Principal principal){
        userRepository.deleteByUsername(principal.getName());
    }

    private UserEntity findByEmail(String email) {

        UserEntity user = userRepository.findByEmail(email);

        if (user == null) {
            throw new ItemNotFoundException("User not found!");
        }

        return user;
    }

    private RoleEntity findRole(String name) {
        RoleEntity role = roleRepository.findByRole(name);

        if (role == null) {
            throw new ItemNotFoundException("Invalid Role");
        }

        return role;
    }

    private UserBindingModel convertUserEntityToUserBM(UserEntity user) {

        UserBindingModel userBindingModel = new UserBindingModel();
        userBindingModel.setUsername(user.getUsername());
        userBindingModel.setEmail(user.getEmail());

        return userBindingModel;
    }

    private void saveUser(UserBindingModel userBindingModel, UserEntity user) {

        RoleEntity role = findRole("USER");

        user.setUsername(userBindingModel.getUsername());
        user.setEmail(userBindingModel.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userBindingModel.getPassword()));
        user.getAuthorities().add(role);

        userRepository.save(user);
    }

    private Date addMinutesToJavaUtilDate(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }
}

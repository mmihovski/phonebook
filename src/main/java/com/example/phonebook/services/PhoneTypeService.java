package com.example.phonebook.services;

import com.example.phonebook.entities.PhoneTypeEntity;
import com.example.phonebook.repositories.PhoneTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Service
@Transactional
public class PhoneTypeService {

    private final PhoneTypeRepository phoneTypeRepository;

    @Autowired
    public PhoneTypeService(PhoneTypeRepository phoneTypeRepository) {
        this.phoneTypeRepository = phoneTypeRepository;
    }

    @PostConstruct
    public void seedPhoneTypesInDb() {
        if (phoneTypeRepository.count() == 0) {

            PhoneTypeEntity phoneTypeEntity1 = new PhoneTypeEntity();
            phoneTypeEntity1.setName("mobile");
            phoneTypeRepository.save(phoneTypeEntity1);

            PhoneTypeEntity phoneTypeEntity2 = new PhoneTypeEntity();
            phoneTypeEntity2.setName("home");
            phoneTypeRepository.save(phoneTypeEntity2);

            PhoneTypeEntity phoneTypeEntity3 = new PhoneTypeEntity();
            phoneTypeEntity3.setName("work");
            phoneTypeRepository.save(phoneTypeEntity3);

            PhoneTypeEntity phoneTypeEntity4 = new PhoneTypeEntity();
            phoneTypeEntity4.setName("other");
            phoneTypeRepository.save(phoneTypeEntity4);
        }
    }
}
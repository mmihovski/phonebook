package com.example.phonebook.utils;

import com.example.phonebook.bindingModels.ContactBindingModel;
import com.example.phonebook.bindingModels.PhoneNumberBindingModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileExporter {

    public static void exportContactsToCsv(HttpServletResponse response, List<ContactBindingModel> contactBindingModelList) {

        String filename = "contacts.csv";

        response.setContentType("text/csv");
        response.setCharacterEncoding("cp1251");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);

        try {

            String id, firstName, lastName, address, email, website, birthday, notes;

            CSVPrinter printer = CSVFormat.DEFAULT.
                    withHeader("#", "first_name", "last_name", "address", "email", "website", "birthday", "notes", "[number, type]").
                    print(response.getWriter());

            for (ContactBindingModel contact : contactBindingModelList) {

                id = String.valueOf(contact.getId());
                firstName = contact.getFirstName();
                lastName = contact.getLastName();
                address = contact.getAddress();
                email = contact.getEmail();
                website = contact.getWebsite();
                birthday = contact.getBirthday();
                notes = contact.getNotes();

                List<String> numbersList = new ArrayList<>();
                for (PhoneNumberBindingModel number : contact.getNumbers()) {
                    numbersList.add(number.getNumber());
                    numbersList.add(number.getType());
                }

                printer.printRecord(id, firstName, lastName, address, email, website, birthday, notes, numbersList);
            }

            printer.flush();
            printer.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void exportContactsToJson(HttpServletResponse response, List<ContactBindingModel> contactBindingModelList) {

        String filename = "contacts.json";

        JsonArray jsonArray = new Gson().toJsonTree(contactBindingModelList).getAsJsonArray();
        String formattedData = new GsonBuilder().setPrettyPrinting().create().toJson(jsonArray);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);

        try {
            PrintWriter writer = response.getWriter();
            writer.print(formattedData);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void exportContactsToXml(HttpServletResponse response, List<ContactBindingModel> contactBindingModelList) {

        String filename = "contacts.xml";

        response.setContentType("application/xml");
        response.setCharacterEncoding("UTF-8");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;


        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document document = dBuilder.newDocument();

            Element rootElement = document.createElement("contacts");
            document.appendChild(rootElement);

            for (ContactBindingModel contact : contactBindingModelList) {
                rootElement.appendChild(createContactChild(document, contact));
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(document);

            StringWriter stringWriter = new StringWriter();
            transformer.transform(source, new StreamResult(stringWriter));

            PrintWriter writer = response.getWriter();
            writer.print(stringWriter.getBuffer().toString());
            writer.flush();
            writer.close();

        } catch (ParserConfigurationException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

    private static Node createContactChild(Document document, ContactBindingModel contact) {

        Element rootElementContact = document.createElement("contact");

        rootElementContact.setAttribute("id", String.valueOf(contact.getId()));

        rootElementContact.appendChild(createElement(document, "firstName", contact.getFirstName()));
        rootElementContact.appendChild(createElement(document, "lastName", contact.getLastName()));

        Element rootElementPhones = document.createElement("phones");

        for (PhoneNumberBindingModel phones : contact.getNumbers()) {

            Element phoneChild = document.createElement("phone");
            phoneChild.appendChild(createElement(document, "number", phones.getNumber()));
            phoneChild.appendChild(createElement(document, "type", phones.getType()));
            rootElementPhones.appendChild(phoneChild);
        }

        rootElementContact.appendChild(rootElementPhones);
        rootElementContact.appendChild(createElement(document, "address", contact.getAddress()));
        rootElementContact.appendChild(createElement(document, "email", contact.getEmail()));
        rootElementContact.appendChild(createElement(document, "website", contact.getWebsite()));
        rootElementContact.appendChild(createElement(document, "birthday", contact.getBirthday()));
        rootElementContact.appendChild(createElement(document, "notes", contact.getNotes()));

        return rootElementContact;
    }

    private static Node createElement(Document document, String name, String value) {
        Element node = document.createElement(name);
        node.appendChild(document.createTextNode(value));

        return node;
    }


}




package com.example.phonebook.validators;

import com.example.phonebook.bindingModels.UserBindingModel;
import com.example.phonebook.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    private UserService userService;

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserBindingModel.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        UserBindingModel user = (UserBindingModel) object;

        if (userService.existsByUsername(user.getUsername())) {
            errors.rejectValue("username", "Duplicate.userForm.username");
        }
        if (userService.existsByEmail(user.getEmail())) {
            errors.rejectValue("email", "Duplicate.userForm.email");
        }
    }
}
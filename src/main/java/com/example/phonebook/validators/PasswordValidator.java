package com.example.phonebook.validators;

import com.example.phonebook.bindingModels.UserBindingModel;
import com.example.phonebook.entities.UserEntity;
import com.example.phonebook.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PasswordValidator implements Validator {

    private UserService userService;

    @Autowired
    public PasswordValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return UserBindingModel.class.equals(aClass);
    }

    @Override
    public void validate(Object object, Errors errors) {
        UserBindingModel userBindingModel = (UserBindingModel) object;

        UserEntity user = userService.findByUsername(userBindingModel.getUsername());

        String existingPassword = user.getPassword();
        String oldPassword = userBindingModel.getOldPassword();
        String newPassword = userBindingModel.getPassword();

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        if (!passwordEncoder.matches(oldPassword, existingPassword)) {
            errors.rejectValue("oldPassword", "Wrong.userForm.password");
        }

        if (passwordEncoder.matches(newPassword, existingPassword)) {
            errors.rejectValue("password", "Same.userForm.password");
        }
    }
}
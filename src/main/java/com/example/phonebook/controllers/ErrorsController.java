package com.example.phonebook.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorsController implements ErrorController {

    @RequestMapping("/error")
    public ModelAndView errorPage(HttpServletRequest request, ModelAndView modelAndView) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        int statusCode = Integer.parseInt(status.toString());

        switch (statusCode) {
            case 400: {
                modelAndView.addObject("message", "Http Error Code: 400. Bad Request");
                break;
            }
            case 401: {
                modelAndView.addObject("message", "Http Error Code: 401. Unauthorized");
                break;
            }
            case 404: {
                modelAndView.addObject("message", "Http Error Code: 404. Resource not found");
                break;
            }
            case 500: {
                modelAndView.addObject("message", "Http Error Code: 500. Internal Server Error");
                break;
            }
            default:
                modelAndView.addObject("message", "Something went wrong");
                break;
        }

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("view", "views/errors/error.html");

        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
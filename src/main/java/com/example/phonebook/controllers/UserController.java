package com.example.phonebook.controllers;

import com.example.phonebook.bindingModels.UserBindingModel;
import com.example.phonebook.services.UserService;
import com.example.phonebook.validators.PasswordValidator;
import com.example.phonebook.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Controller
public class UserController {

    private final UserService userService;
    private final UserValidator userValidator;
    private final PasswordValidator passwordValidator;

    @Autowired
    public UserController(UserService userService, UserValidator userValidator, PasswordValidator passwordValidator) {
        this.userService = userService;
        this.userValidator = userValidator;
        this.passwordValidator = passwordValidator;
    }

    @GetMapping("/login")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getLogin(@ModelAttribute("userBindingModel") UserBindingModel user, ModelAndView modelAndView) {

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("userBindingModel", user);
        modelAndView.addObject("view", "views/account/login.html");

        return modelAndView;
    }

    @GetMapping("/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getRegister(@ModelAttribute("userBindingModel") UserBindingModel user, ModelAndView modelAndView) {

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("userBindingModel", user);
        modelAndView.addObject("view", "views/account/register.html");

        return modelAndView;
    }

    @PostMapping("/register")
    @PreAuthorize("isAnonymous()")
    public ModelAndView postRegister(@ModelAttribute("userBindingModel") UserBindingModel user, BindingResult bindingResult, ModelAndView modelAndView) {

        userValidator.validate(user, bindingResult);

        if (!bindingResult.hasErrors()) {
            userService.registerUser(user);
            modelAndView.setViewName("redirect:/login");

        } else {
            modelAndView.setViewName("fragments/layout.html");
            modelAndView.addObject("userBindingModel", user);
            modelAndView.addObject("view", "views/account/register.html");
        }

        return modelAndView;
    }

    @GetMapping("/account")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getAccount(ModelAndView modelAndView, Principal principal) {

        UserBindingModel user = userService.getUserByUsername(principal.getName());

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("view", "views/account/account.html");
        modelAndView.addObject("userBindingModel", user);

        return modelAndView;
    }

    @PostMapping("/account")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView postAccount(@ModelAttribute("userBindingModel") UserBindingModel user, BindingResult bindingResult, ModelAndView modelAndView) {

        passwordValidator.validate(user, bindingResult);

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("userBindingModel", user);
        modelAndView.addObject("view", "views/account/account.html");

        if (!bindingResult.hasErrors()) {
            userService.changePassword(user);
            modelAndView.addObject("message", "Password has changed successfully");
        }

        return modelAndView;
    }

    @GetMapping("/forgotten-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getForgottenPassword(@ModelAttribute("userBindingModel") UserBindingModel user, ModelAndView modelAndView) {

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("userBindingModel", user);
        modelAndView.addObject("view", "views/account/forgotten-password.html");

        return modelAndView;
    }

    @PostMapping("/forgotten-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView postForgottenPassword(@ModelAttribute("userBindingModel") UserBindingModel user, ModelAndView modelAndView) {

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("userBindingModel", user);
        modelAndView.addObject("view", "views/account/forgotten-password.html");

        if (!userService.existsByEmail(user.getEmail())) {
            modelAndView.addObject("message", "User with this email address does not exist");
        } else {
            userService.createTokenAndSendEmail(user.getEmail());
            modelAndView.addObject("message", "The email has been sent.\nPlease follow the instructions");
        }

        return modelAndView;
    }

    @GetMapping("/reset-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView getResetPassword(@RequestParam("token") String token, ModelAndView modelAndView) {

        modelAndView.setViewName("fragments/layout.html");

        if (userService.isTokenValid(token)) {
            UserBindingModel user = userService.getUserByToken(token);
            userService.deleteToken(token);

            modelAndView.addObject("userBindingModel", user);
            modelAndView.addObject("view", "views/account/reset-password.html");
        } else {
            modelAndView.addObject("message", "The link is invalid or broken!");
            modelAndView.addObject("view", "views/errors/error.html");
        }

        return modelAndView;
    }

    @PostMapping("/reset-password")
    @PreAuthorize("isAnonymous()")
    public ModelAndView postResetPassword(@ModelAttribute("userBindingModel") UserBindingModel user, ModelAndView modelAndView) {

        userService.changePassword(user);

        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("message", "The password has changed successfully!");
        modelAndView.addObject("view", "views/errors/error.html");
        return modelAndView;
    }

    @GetMapping("/account/delete")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView deleteAccount(HttpServletRequest request, HttpServletResponse response, ModelAndView modelAndView, Principal principal) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
            userService.deleteUser(principal);
            modelAndView.setViewName("redirect:/login?logout");
        } else {
            modelAndView.setViewName("fragments/layout.html");
            modelAndView.addObject("message", "Something went wrong");
        }

        return modelAndView;
    }
}
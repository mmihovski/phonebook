package com.example.phonebook.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class HomeController {

    @GetMapping("/")
    public ModelAndView index(Principal principal, ModelAndView modelAndView) {
        modelAndView.setViewName("fragments/layout.html");
        modelAndView.addObject("view", "index.html");

        if (principal != null) {
            modelAndView.addObject("username", principal.getName());
        }

        return modelAndView;
    }
}

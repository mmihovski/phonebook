package com.example.phonebook.controllers;

import com.example.phonebook.bindingModels.ContactBindingModel;
import com.example.phonebook.services.ContactsService;
import com.example.phonebook.services.StorageService;
import com.example.phonebook.utils.FileExporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;

@Controller
public class ContactsController {

    private final ContactsService contactsService;
    private final StorageService storageService;

    @Autowired
    public ContactsController(ContactsService contactsService, StorageService storageService) {
        this.contactsService = contactsService;
        this.storageService = storageService;
    }

    @GetMapping("/contacts")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getAllContacts(Principal principal, ModelAndView modelAndView) {
        modelAndView.setViewName("fragments/layout");
        modelAndView.addObject("view", "views/contacts/all-contacts.html");

        List<ContactBindingModel> contacts = contactsService.getAllContacts(principal);
        modelAndView.addObject("contacts", contacts);

        return modelAndView;
    }

    @GetMapping("/contacts/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView getContact(@PathVariable int id, ModelAndView modelAndView) {

        ContactBindingModel contact = contactsService.getContactById(id);

        modelAndView.setViewName("fragments/layout");
        modelAndView.addObject("view", "views/contacts/contact-details.html");
        modelAndView.addObject("contact", contact);
        if(contact.getPictureName() != null && !contact.getPictureName().isEmpty()) {
            String filePath = storageService.loadPath(contact.getPictureName());
            modelAndView.addObject("filePath", filePath);
        }
        return modelAndView;
    }

    @GetMapping(value = "/contacts/searching", params = "name")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView searchContact(@RequestParam String name, ModelAndView modelAndView) {

        List<ContactBindingModel> persons = contactsService.searchContact(name);

        modelAndView.setViewName("fragments/layout");
        modelAndView.addObject("view", "views/contacts/all-contacts.html");
        modelAndView.addObject("contacts", persons);

        return modelAndView;
    }

    @GetMapping(value = "/contacts/sorting", params = "criteria")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView sortContacts(@RequestParam String criteria, ModelAndView modelAndView, Principal principal) {

        List<ContactBindingModel> persons;

        switch (criteria) {
            case "first_name":
                persons = contactsService.getAllContactsSortByFirstName(principal);
                break;
            case "last_name":
                persons = contactsService.getAllContactsSortByLastName(principal);
                break;
            default:
                persons = contactsService.getAllContacts(principal);
        }

        modelAndView.setViewName("fragments/layout");
        modelAndView.addObject("view", "views/contacts/all-contacts.html");
        modelAndView.addObject("contacts", persons);

        return modelAndView;
    }

    @GetMapping("/contacts/create")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView createContact(@ModelAttribute("contactBindingModel") ContactBindingModel contact, Principal principal, ModelAndView modelAndView) {

        contact.setUsername(principal.getName());
        List<String> phoneTypes = contactsService.findAllPhoneTypes();

        modelAndView.setViewName("fragments/layout");
        modelAndView.addObject("view", "views/contacts/create-contact.html");
        modelAndView.addObject("contactBindingModel", contact);
        modelAndView.addObject("phoneTypes", phoneTypes);

        return modelAndView;
    }

    @PostMapping("/contacts/create")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView createContact(@ModelAttribute("contactBindingModel") ContactBindingModel contact, ModelAndView modelAndView, Principal principal) {

        contactsService.createContact(contact);

        modelAndView.setViewName("redirect:/contacts");

        return modelAndView;
    }

    @GetMapping("/contacts/update/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView updateContact(@PathVariable int id, ModelAndView modelAndView, Principal principal) {

        ContactBindingModel contact = contactsService.getContactById(id);
        contact.setUsername(principal.getName());

        List<String> phoneTypes = contactsService.findAllPhoneTypes();

        modelAndView.setViewName("fragments/layout");
        modelAndView.addObject("view", "views/contacts/update-contact.html");
        modelAndView.addObject("phoneTypes", phoneTypes);
        modelAndView.addObject("contactBindingModel", contact);

        return modelAndView;
    }

    @PostMapping("/contacts/update/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView updateContact(@PathVariable int id, @ModelAttribute("contactBindingModel") ContactBindingModel contact, ModelAndView modelAndView, Principal principal) {

        contactsService.updateContact(id, contact);

        modelAndView.setViewName("redirect:/contacts");

        return modelAndView;
    }

    @GetMapping("/contacts/delete/{id}")
    //@DeleteMapping("/contacts/delete/{id}")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView deleteContact(@PathVariable int id, ModelAndView modelAndView, Principal principal) {


        contactsService.deleteContact(id);

        modelAndView.setViewName("redirect:/contacts");

        return modelAndView;
    }

    @GetMapping(value = "/contacts/export/csv")
    public void exportToCsv(HttpServletResponse response, Principal principal) {
        FileExporter.exportContactsToCsv(response, contactsService.getAllContacts(principal));
    }

    @GetMapping(value = "/contacts/export/json")
    public void exportToJson(HttpServletResponse response, Principal principal) {
        FileExporter.exportContactsToJson(response, contactsService.getAllContacts(principal));
    }

    @GetMapping(value = "/contacts/export/xml")
    public void exportToXml(HttpServletResponse response, Principal principal) {
        FileExporter.exportContactsToXml(response, contactsService.getAllContacts(principal));
    }
}

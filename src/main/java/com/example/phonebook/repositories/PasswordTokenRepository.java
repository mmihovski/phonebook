package com.example.phonebook.repositories;

import com.example.phonebook.entities.PasswordToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordTokenRepository extends JpaRepository<PasswordToken, Integer> {
    PasswordToken findByConfirmationToken(String confirmationToken);
    void deleteByConfirmationToken(String confirmationToken);
}

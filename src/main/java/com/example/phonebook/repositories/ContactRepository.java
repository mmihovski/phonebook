package com.example.phonebook.repositories;

import com.example.phonebook.entities.ContactEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<ContactEntity, Integer> {
    List<ContactEntity> findAllByOwnerUsername(String username);
    List<ContactEntity> findAllByOwnerUsernameOrderByFirstName(String username);
    List<ContactEntity> findAllByOwnerUsernameOrderByLastName(String username);

    @Query(value = "select * from contacts c where c.first_name like %:name% or c.last_name like %:name%", nativeQuery = true)
    List<ContactEntity> findAllByName(@Param(value = "name") String name);
}

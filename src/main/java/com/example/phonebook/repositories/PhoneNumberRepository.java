package com.example.phonebook.repositories;

import com.example.phonebook.entities.ContactEntity;
import com.example.phonebook.entities.PhoneNumberEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface PhoneNumberRepository extends JpaRepository<PhoneNumberEntity, Integer> {
    void deleteAllByOwner(ContactEntity person);
}

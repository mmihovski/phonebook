package com.example.phonebook.repositories;

import com.example.phonebook.entities.PhoneTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhoneTypeRepository extends JpaRepository<PhoneTypeEntity, Integer> {
    PhoneTypeEntity findByName(String name);
}

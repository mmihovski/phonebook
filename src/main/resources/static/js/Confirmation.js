/**
 * @return {boolean}
 */
function ConfirmDelete() {
    return confirm("Are you sure you want to delete?");
}

function logout() {
    document.logoutForm.submit();
}

function rowClicked(value) {
    location.href = "/contacts/" + value;
}


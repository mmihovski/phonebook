function loginValidation() {

    var username, password;
    var isValid = true;
    var message = "This field is required";

    var error = document.getElementsByClassName("message");
    for (var i = 0; i < error.length; i++) {
        error[i].innerHTML = "";
    }

    username = document.getElementById("username").value;
    password = document.getElementById("password").value;

    if (password === "") {
        document.getElementById("password-error").innerHTML = message;
        isValid = false;
    }
    if (username === "") {
        document.getElementById("username-error").innerHTML = message;
        isValid = false;
    }

    return isValid;
}

function registerValidation() {
    var username, email, password, confirmPassword;
    var isValid = true;

    var passwordMessage = "Password must contain at least one number, one uppercase and lowercase letter, " +
        "and at least 8 or more characters.";
    var confirmPasswordMessage = "The passwords do not match!";
    var usernameMessage = "Username can only contains alphanumeric characters, underscore and dot. " +
        "Number of characters must be between 8 to 20.";
    var emailMessage = "Please insert a valid e-mail address.";

    var usernameRegex = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$";
    var passwordRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}";

    var emailRegex = "^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";

    var error = document.getElementsByClassName("message");
    for (var i = 0; i < error.length; i++) {
        error[i].innerHTML = "";
    }

    username = document.getElementById("username").value;
    email = document.getElementById("email").value;
    confirmPassword = document.getElementById("confirm-password").value;
    password = document.getElementById("password").value;

    if (!username.match(usernameRegex)) {
        document.getElementById("username-error").innerHTML = usernameMessage;
        isValid = false;
    }
    if (!email.match(emailRegex)) {
        document.getElementById("email-error").innerHTML = emailMessage;
        isValid = false;
    }
    if (!password.match(passwordRegex)) {
        document.getElementById("password-error").innerHTML = passwordMessage;
        isValid = false;
    }
    if (confirmPassword !== password) {
        document.getElementById("confirm-password-error").innerHTML = confirmPasswordMessage;
        isValid = false;
    }

    return isValid;
}

function changePasswordValidation() {
    var oldPassword, newPassword, confirmNewPassword;
    var isValid = true;

    var message = "This field is required";
    var passwordMessage = "Password must contain at least one number, one uppercase and lowercase letter, " +
        "and at least 8 or more characters.";
    var confirmPasswordMessage = "The passwords do not match!";

    var passwordRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}";

    var error = document.getElementsByClassName("message");
    for (var i = 0; i < error.length; i++) {
        error[i].innerHTML = "";
    }

    confirmNewPassword = document.getElementById("confirm-password").value;
    newPassword = document.getElementById("new-password").value;
    oldPassword = document.getElementById("old-password").value;

    if (oldPassword === "") {
        document.getElementById("old-password-error").innerHTML = message;
        isValid = false;
    }
    if (!newPassword.match(passwordRegex)) {
        document.getElementById("new-password-error").innerHTML = passwordMessage;
        isValid = false;
    }
    if (confirmNewPassword !== newPassword) {
        document.getElementById("confirm-password-error").innerHTML = confirmPasswordMessage;
        isValid = false;
    }

    return isValid;
}

function resetPasswordValidation() {
    var newPassword, confirmNewPassword;
    var isValid = true;

    var message = "This field is required";
    var passwordMessage = "Password must contain at least one number, one uppercase and lowercase letter, " +
        "and at least 8 or more characters.";
    var confirmPasswordMessage = "The passwords do not match!";

    var passwordRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}";

    var error = document.getElementsByClassName("message");
    for (var i = 0; i < error.length; i++) {
        error[i].innerHTML = "";
    }

    confirmNewPassword = document.getElementById("confirm-password").value;
    newPassword = document.getElementById("new-password").value;

    if (!newPassword.match(passwordRegex)) {
        document.getElementById("new-password-error").innerHTML = passwordMessage;
        isValid = false;
    }
    if (confirmNewPassword !== newPassword) {
        document.getElementById("confirm-password-error").innerHTML = confirmPasswordMessage;
        isValid = false;
    }

    return isValid;
}

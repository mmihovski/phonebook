function contactValidation() {

    var firstName, lastName, email, website, birthday, notes;
    var isValid = true;

    var message = "This field is required";
    var emailMessage = "Please insert a valid e-mail address.";
    var websiteMessage = "Please insert a valid website URL.";
    var birthdayMessage = "Please insert a valid date";
    var notesMessage = "The notes is too long. Please insert text up to 1000 characters!";

    var emailRegex = "^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
    var websiteRegex = "^[\\w.-]+(?:\\.[\\w\\.-]+)+[\\w\\-\\._~:/?#[\\]@!\\$&'\\(\\)\\*\\+,;=.]+$";
    var dateRegex = "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))";

    firstName = document.getElementById("first-name").value;
    lastName = document.getElementById("last-name").value;
    email = document.getElementById("email").value;
    website = document.getElementById("website").value;
    birthday = document.getElementById("birthday").value;
    notes = document.getElementById("notes").value;

    var error = document.getElementsByClassName("message");
    for (var i = 0; i < error.length; i++) {
        error[i].innerHTML = "";
    }

    if (firstName === "") {
        document.getElementById("first-name-error").innerHTML = message;
        isValid = false;
    }
    if (lastName === "") {
        document.getElementById("last-name-error").innerHTML = message;
        isValid = false;
    }
    if (email !== "" && !email.match(emailRegex)) {
        document.getElementById("email-error").innerHTML = emailMessage;
        isValid = false;
    }
    if (website !== "" && !website.match(websiteRegex)) {
        document.getElementById("website-error").innerHTML = websiteMessage;
        isValid = false;
    }
    if (birthday !== "" && !birthday.match(dateRegex)) {
        document.getElementById("birthday-error").innerHTML = birthdayMessage;
        isValid = false;
    }
    if(notes !== "" && notes.length > 1000){
        document.getElementById("notes-error").innerHTML = notesMessage;
        isValid = false;
    }

    return isValid;
}
